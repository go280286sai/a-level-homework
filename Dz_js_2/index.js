/*HW1: Есть массив ['Капуста', 'Репа', 'Редиска', 'Морковка'].
Надо вывести в консоль строку 'Капуста | Репа | Редиска | Морковка';*/
let arr = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
console.log(arr.join('|'));

/*Есть строка 'Вася;Петя;Вова;Олег'. Используя стандартные методы строк получить массив их имен;*/
let str = 'Вася;Петя;Вова;Олег';
console.log(str.split(';'));

/*HW3: Напишите функцию hello2(), которая при вызове будет принимать переменную (в аргументы) name (например, «Василий»)
и выводить строку (в нашем случае «Привет, Василий»).
В случае отсутствующего аргумента выводить «Привет, гость»*/
/**
 *
 * @param name
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function hello2(name = 'гость') {
    console.log('Привет, ' + name);
}

hello2('alex');
hello2();

/*Есть массив ['яблоко', 'ананас', 'груша']
Привести каждый элемент массива в верхний регистр (сделать все слово большими буквами) и получить результат
(новый массив) в новую переменную.*/
const fruits = ['яблоко', 'ананас', 'груша'];
const fruitsInUpperCase = [];
for (el of fruits) {
    fruitsInUpperCase.push(el.toUpperCase());
}
console.log(fruitsInUpperCase);

/*HW5: Написать функцию addOneForAll, которая может принять неограниченное кол-во аргументов.
Добавить к каждому аргументу 1 и вернуть новый массив с новыми значениями.*/
/**
 *
 * @param nums
 * @returns array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function addOneForAll(...nums) {
    let arr = nums.map(el => el + 1);
    return arr;
}

console.log(addOneForAll(1, 2, 3, 4, 5, 6));

/*Написать функцию getSum, которая может принять неограниченное кол-во аргументов и возвращает их сумму.*/
/**
 *
 * @param nums
 * @returns array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getSum(...nums) {
    let func = (a, b) => (a + b);
    return nums.reduce(func);
}

console.log(getSum(1, 2, 3, 4, 5, 6));

/*Есть массив [1, 'hello', 2, 3, 4, '5', '6', 7, null]. Отфильтровать массив так, чтобы остались только числа.
Сделать можно любым способом из того, что учили.*/
let arr1 = [1, 'hello', 2, 3, 4, '5', '6', 7, null];
let arr2 = arr1.filter(el => typeof (el) === 'number');
console.log(arr2);

/*Написать функцию arrayTesting, которая принимает в себя любой массив (в аргументы)
функция проверяет есть ли в массиве хоть одно true значение
и если оно есть, то возвращаем из функции строку 'Нашли true значение', если его нет - 'Ничего нет'*/
/**
 *
 * @param nums
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function arrayTesting(nums) {
    let result = nums.some(el => el == true); // true
    result === true ? console.log('Нашли true значение') : console.log('Ничего нет');
};
const haveTrueValue = arrayTesting([0, false, null, 1]);
const dontHaveTrueValue = arrayTesting([0, false, null, 0]);
