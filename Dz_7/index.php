<?php
/**
 * Recipe class file
 *
 * PHP Version 7.4
 *
 * @category Recipe
 * @package  Recipe
 * @author   Alexander Storchak <go280286sai@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://example.com/recipes
 */

/**
 * Get element from array
 *
 * @param array $arr post array
 *
 * @return void
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}

echo '<br>1. Найти минимальное и максимальное среди 3 чисел.<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 * 
 * @param int $a
 * @param int $b
 * @param int $c
 *
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function (int $a,int $b,int $c):string {
    return ($a > $b && $b > $c) ? ('max: ' . $a . ' min: ' . $c) :
        (($a > $c && $c > $b ? ('max: ' . $a . ' min: ' . $b) :
            (($b > $a && $a > $c) ? ('max: ' . $b . ' min: ' . $c) :
                (($b > $c && $c > $a) ? ('max: ' . $b . ' min: ' . $a) :
                    (($c > $a && $a > $b) ? ('max: ' . $c . ' min: ' . $a) :
                        ('max: ' . $c . ' min: ' . $a))))));
};
echo $myFunc(1, 2, 3);
echo '<br>';
echo $myFunc(2, 1, 3);
echo '<br>';
echo $myFunc(3, 2, 1);
echo '<br>';
echo $myFunc(3, 1, 2);
echo '<br>';
echo $myFunc(1, 3, 2);
echo '<br>';
echo $myFunc(2, 3, 1);
echo '<br>';
echo $myFunc(1, 3, 2);
echo '<br>';
echo $myFunc(1, 2, 3);
echo '<br>';
echo '<br>b. Стрелочные функции<br>';
/**
 * Arrow function
 *
 * @param int $a
 * @param int $b
 * @param int $c
 *
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = fn($a, $b, $c) => ($a > $b && $b > $c) ? ('max: ' . $a . ' min: ' . $c) :
    (($a > $c && $c > $b ? ('max: ' . $a . ' min: ' . $b) :
        (($b > $a && $a > $c) ? ('max: ' . $b . ' min: ' . $c) :
            (($b > $c && $c > $a) ? ('max: ' . $b . ' min: ' . $a) :
                (($c > $a && $a > $b) ? ('max: ' . $c . ' min: ' . $a) :
                    ('max: ' . $c . ' min: ' . $a))))));
echo $myFunc(2, 1, 3);
echo '<br>';
echo $myFunc(3, 2, 1);
echo '<br>';
echo $myFunc(3, 1, 2);
echo '<br>';
echo $myFunc(1, 3, 2);
echo '<br>';
echo $myFunc(2, 3, 1);
echo '<br>';
echo $myFunc(1, 3, 2);
echo '<br>';
echo $myFunc(1, 2, 3);
echo '<br>';
echo '<hr>';
echo '<br>2. Найти площадь<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 *
 * @param int $a
 * @param int $b
 *
 * @return int
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function (int $a, int $b): int {
    return ($a + $b) * 2;
};
echo $myFunc(2, 4);
echo '<br>b. Стрелочные функции<br>';
/**
 * Arrow function
 *
 * @param int $a
 * @param int $b
 *
 * @return int
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = fn($a, $b) => ($a + $b) * 2;
echo $myFunc(2, 4);
echo '<hr>';
echo '<br>3. Теорема Пифагора<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 *
 * @param int $a
 * @param int $b
 *
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function (int $a,int $b): int {
    return ($a ** 2 + $b ** 2) ** (1 / 2);
};
echo $myFunc(6, 8);
echo '<br>b. Стрелочные функции<br>';
/**
 * Arrow function
 *
 * @param int $a
 * @param int $b
 *
 * @return int
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = fn($a, $b) => ($a ** 2 + $b ** 2) ** (1 / 2);
echo $myFunc(6, 8);
echo '<hr>';
echo '<br>4. Найти периметр<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 *
 * @param int $a
 * @param int $b
 * @param int $c
 *
 * @return int
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function (int $a,int $b,int $c): int {
    return $a + $b + $c;
};
echo $myFunc(6, 8, 6);
echo '<br>b. Стрелочные функции<br>';
/**
 * Arrow function
 *
 * @param int $a
 * @param int $b
 * @param int $c
 *
 * @return int
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = fn($a, $b, $c) => ($a + $b + $c);
echo $myFunc(6, 8, 6);
echo '<hr>';
echo '<br>5. Найти дискриминант<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 *
 * @param int $a
 * @param int $b
 * @param int $c
 *
 * @return int
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function (int $a,int $b,int $c): int {
    return ($b ** 2 - 4 * $a * $c);
};
echo $myFunc(2, 4, 2);
echo '<br>b. Стрелочные функции<br>';
/**
 * Arrow function
 *
 * @param int $a
 * @param int $b
 * @param int $c
 *
 * @return int
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = fn($a, $b, $c) => ($b ** 2 - 4 * $a * $c);
echo $myFunc(2, 4, 2);
echo '<hr>';
echo '<br>6. Создать только четные числа до 100<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 *
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function (): array {
    $arr = [];
    for ($i = 0; $i <= 100; $i++) {
        if ($i % 2 == 0) {
            $arr[] = $i;
        }
    }
    return $arr;
};
getArrayView($myFunc());
echo '<br>b. Стрелочные функции<br>';
/**
 * Arrow function
 *
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = fn() => [range(0, 100, 2)];
getArrayView($myFunc());
echo '<hr>';
echo '<br>7. Создать нечетные числа до 100<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 *
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function (): array {
    $arr = [];
    for ($i = 0; $i <= 100; $i++) {
        if ($i % 2 != 0) {
            $arr[] = $i;
        }
    }
    return $arr;
};
getArrayView($myFunc());
echo '<br>b. Стрелочные функции<br>';
/**
 * Arrow function
 *
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = fn() => [range(1, 100, 2)];
getArrayView($myFunc());
echo '<hr>';
echo '<br>8. Определите, есть ли в массиве повторяющиеся элементы.<br>';
echo '<br>a. Анонимные функции<br>';
/**
 * Anonymous function
 *
 * @param $arr
 *
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$myFunc = function ($arr) {
    $k = 0;
    $mas = [];
    $count = count($arr);
    while ($k != $count) {
        $t = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($arr[$k] == $arr[$i]) {
                $t++;
            }
        }
        if ($t > 1 && !in_array($arr[$k], $mas)) {
            $mas[] = $arr[$k];
        }
        $k++;
    }
    return $mas;
};
getArrayView(myArray([1, 2, 3, 1, 2, 5, 8, 7, 5, 2, 4, 1, 2]));
echo '<br>b. Стрелочные функции<br>';
/**
 * Anonymous function
 *
 * @param array $arr get array
 *
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function myArray($arr): array
{
    $k = 0;
    $mas = [];
    $count = count($arr);
    while ($k != $count) {
        $t = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($arr[$k] == $arr[$i]) {
                $t++;
            }
        }
        if ($t > 1 && !in_array($arr[$k], $mas)) {
            $mas[] = $arr[$k];
        }
        $k++;
    }
    return $mas;
}


/**
 * Arrow function
 *
 * @param $arr
 *
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$arr = [1, 2, 3, 1, 2, 5, 8, 7, 5, 2, 4, 1, 2];
$myFunc = fn($arr) => (count(array_unique($arr))) == count($arr) ?
    'Повторов нет' : 'Есть повторяющиеся элементы';
echo $myFunc($arr);
