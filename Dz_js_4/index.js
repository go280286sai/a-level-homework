/*1. Написать функцию bindFunc, которая принимает в себя 2 + аргументов (Точно должна принять 2 аргумента, а дальше сколько угодно).
1 аргумент - какая-то функция
2 аргумент - значение контекста
3 + ... аргументы - любое кол-во аргументов
Эта функция, должна устанавливать контекст для функции, которая в первом аргументе, и возвращать эту функцию с новым контекстом.
Сам контекст, который мы хотим установить, находиться во втором аргументе*/
let person = {
    firstName: "Alex",
    age:35,
    bindFunc(word) {
        console.log(`${word}, I am ${this.firstName}! me is ${this.age} old`);
    }
};
let user = person.bindFunc.bind(person);
user("Hello");
user("Goodby");

/*2. Написать функцию, которая не принимает никаких аргументов. В теле функции написать логику для нахождения суммы
значений любого количества ключей (значения ключей должны быть больше нуля) из переданного контекста.
Обращаться к objectA напрямую нельзя :)*/
const func = {
    run: function () {
        let arr = Object.values(objectA);
        let total = 0;
        for (item of arr) {
            total += item;
        }
        return total;
    }
}
const objectA = {
    a: 1,
    b: 2,
    c: 3,
    d: 4,
}
console.log();
func.__proto__ = objectA;
console.log(func.run());

/*3. Написать функцию, которая возвращает новый массив, в котором должны быть только четные числа, которые больше двуx
и меньше 10. Новый массив будет состоять из значений ключа values из контекста, если такого ключа нет, то выводим
сообщение "Не найдено".
Обращаться к valObject0 напрямую нельзя :)
Если хотите использовать map, то внутри map this всегда равен глобальному объекту. Чтобы это поменять передаем нужное
значение this во второй аргумент map -
arr.map(() => {}, this); */
function getNewArray() {
    //this.__proto__ = valObject0;
    return this.values.filter(el => (el > 2 && el < 10 && typeof (el) === 'number'));
};
const valObject0 = {
    values: [1, '2', 4, 8, '8', 3, 10, null, false],
};
let result = getNewArray.call(valObject0);
console.log(result)
