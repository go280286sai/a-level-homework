/*Если в переменную вписана строка , то выводим в консоль сообщение "Привет, ЗНАЧНИЕ_NAME" Если в переменную вписана не' +
' строка, а любой другой тип данных, то выводим сообщение "Ошибка, не тот тип данных"*/
const name = 'alex';
if (typeof (name) == 'string') {
    console.log('Hello ' + name);
} else {
    console.log("Ошибка, не тот тип данных");
}

const age = 30;
if (typeof (age) == 'string') {
    console.log('Hello ' + age);
} else {
    console.log("Ошибка, не тот тип данных");
}

//Вывести в консоль примеры всех типо данных
console.log(typeof (12345));
console.log(typeof ('String'));
console.log(typeof (true));
let a=null;
console.log(a);
console.log(typeof (a));
console.log(typeof (c));
let b={
    name:'name'
};
console.log(typeof (b));
console.log(typeof (122346309974630498573406875046345563534n));