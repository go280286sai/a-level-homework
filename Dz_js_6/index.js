/*1. Создать функцию конструктор Animal c аргументами name, age, color. Написать логику для того, чтобы функцию можно
было вызывать как с, так и без new:
При вызове без new новый обьект все равно должен создаться*/
function person(name, age, color) {
    this.name = name;
    this.age = age;
    this.color = color;
};

function Animal(name, age, color) {
    return new person(name, age, color);
}

const rabbit = new Animal('Name', 'Age', 'Colour');
console.log(rabbit);
console.log(typeof (rabbit));
const rabbit1 = Animal('Name', 'Age', 'Colour');
console.log(rabbit1);
console.log(typeof (rabbit1));

/*2. Создайте функцию-конструктор Calculator, который создаёт объекты с такими методами:
read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
setAction() запрашивает действие при помощи prompt, которые мы хотим сделать (+, -, / и т.д)
doAction() выполняет действие, которое юзер ввел (будет вызывать в себя методы sum, mul, min и т.д)
sum() возвращает сумму введённых свойств.
mul() возвращает произведение введённых свойств.
min() возвращает разницу введённых свойств.
другие методы можете добавит если хотите (метод для квадратного корня и т.д.)
const calculator = new Calculator();
calculator.read();
calculator.setAction();
const tres = calculator.doAction(); // результат*/
function Calculator() {
    this.read = function () {
        let a = prompt('Введите ключь');
        let b = prompt('Введите значение');
        return `{${a}:${b}}`;
    };
    this.setAction = function () {
        let a = prompt('Введите первое значение');
        let b = prompt('Введите арифмитическую операцию');
        let c = prompt('Введите второе значение');
        switch (true) {
            case b == '*': {
                return a * c;
                break;
            }
            case b == '/': {
                return a / c;
                break;
            }
            case b == '+': {
                return a + c;
                break;
            }
            case b == '-': {
                return a - c;
                break;
            }
            default: {
                return 'Wrong';
                break;
            }

        }

    }
    this.doAction = function () {
        let a = prompt('Введите первое значение');
        let b = prompt('Введите арифмитическую операцию');
        let c = prompt('Введите второе значение');
        switch (true) {
            case b == '*': {
                return this.mul(a, c);
                break;
            }
            case b == '/': {
                return this.del(a, c);
                break;
            }
            case b == '+': {
                return this.sum(a, c);
                break;
            }
            case b == '-': {
                return this.diff(a, c);
                break;
            }
            default: {
                return 'Wrong';
                break;
            }

        }

    };
    this.sum = function (a, c) {
        return a + c;
    }
    ; //возвращает сумму введённых свойств.
    this.mul = function (a, c) {
        return a * c;
    }
    this.del = function (a, c) {
        return a / c;
    }
    this.diff = function (a, c) {
        return a - c;
    }
};
// const ner=new Calculator();
// console.log(ner.read());

// const ner=new Calculator();
// console.log(ner.setAction());

// const ner=new Calculator();
// console.log(ner.doAction());

/*3. Создать функцию конструктор Nums, которая принимает бесконечное множество аргументов, и они записываются в свойство args в виде массива
Добавить в прототип для всех объектов, которые создадим от конструктора Nums, 2 метода:
метод getSum должен вернуть сумму всех элементов (которые только целые числа) массива args
метод myFilterReverse должен отфильтровать массив и оставить только целые числа и развернуть массив (было [1, 2, 3] -> стало [3, 2, 1])
Метод .reverse использовать нельзя :)
только целые числа -> Number.isInteger(1); // true Number.IsInteger(1.2); // false*/
function Nums(...args) {
    let total = (a, b) => a + b;
    this.getSum = args.filter(el => Number.isInteger(el) == true).reduce(total);
    this.MyFilterRevers = args.filter(el => Number.isInteger(el) == true).sort((el1, el2) => {
        if (el1 == el2) {
            return 0;
        } else if (el1 < el2) {
            return 1;
        } else if (el1 > el2) {
            return -1;
        }

    });
}
const object4 = new Nums(1, 2, 6.7, 3, 'f');
console.log(object4.getSum);
console.log(object4.MyFilterRevers);

/*4. Есть массив [1, 1, 2, 2, 3]
Создать свой метод getUnique для любых массивов, который отфильтрует массив и оставит в нем только уникальные значения
Подсказка: чтобы было легче почитайте про метод .includes()

const newArr = arr.getUnique(); //  [1, 2, 3]*/
function getUnique(arr){
    this.arr = arr;
    let mas=[];
    for(let i=0; i<this.arr.length; i++){
        if(mas.includes(this.arr[i])!=true){
            mas.push(this.arr[i]);
        }
    }
    return mas;
};
const arr = [1, 1, 2, 2, 3];
const masArray = new getUnique(arr);
console.log(masArray);

/*5. Есть объект {a: 1, b: 2, c: 3, d: false, e: 0}; Нужно создать 2 метода для любых объектов:
метод getKeySum, который найдет сумму значений всех ключей, которые true.
метод reversKey который поменяет местави key и value (ключ и значение)
Пример Был объект {a: 1, b: 2}.reversKey() -> стало {1: 'b', 2: 'a'}*/
const obj = {a: 1, b: 2, c: 3, d: false, e: 0};
function getKeySum(arr){
    let total=0;
    for(el in arr){
        if(arr[el]==true || arr[el]!=0){
            total+=arr[el];
        }
    }
    console.log(total);
};
function reversKey(obj){
    const res = {};
    Object.keys(obj).forEach(function(value) {
        var key = obj[value];
        res[key] = value;
    });
    console.log(res);
};
getKeySum.call(this, obj);
reversKey.call(this, obj);