<?php
/**
 * @param array $array
 * @return mixed
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function dd($array)
{
    if ($array) {
        foreach ($array as $item) {
            echo $item . '<br>';
        }
        return dd($array = []);
    } else {
        return 1;
    }
}

$array = [123, 456, 789, 753];
dd($array);
