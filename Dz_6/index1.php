<?php
/**
 * @param array $arr
 * @return mixed
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}

/**
 * @param array $arr
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getCountArr($arr): float
{
    $getCount = 0;
    for ($i = 0; $arr[$i] != null; $i++) {
        $getCount++;
    }
    return $getCount;
}

echo '1. Создать функцию по нахождению числа в степени<br>';
echo 'Пользовательская функция<br>';
/**
 * @param float $n
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getPowNunber($n): float
{
    return $n ** $n;
}

echo getPowNunber(5) . '<br>';
echo 'Рекурсия<br>';
/**
 * @param float $n
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getPowrecursion($n): float
{
    if ($n) {
        return $n ** $n;
    } else {
        return 1;
    }
}

echo getPowrecursion(5) . '<br>';
echo 'anonymous<br>';
/**
 * @param float $n
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$anonymous = function ($n) {
    return $n ** $n;
};
echo $anonymous(5) . '<br>';
/**
 * @param float $a
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaPowrecursion($a, $n): string
{
    if (getPowrecursion($a) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}

echo getProverkaPowrecursion(5, 3125);
echo '<br>';
echo '2. Написать функцию сортировки. Функция принимает массив случайных чисел и сортирует их по порядку. По дефолту функция сортирует в порядке возрастания. Но если передать в сторой параметр то функция будет сортировать по убыванию.
<br>';
echo 'Пользовательская функция<br>';
/**
 * @param array  $arr
 * @param string $sort
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getSortUser($arr, $sort = 'desc'): array
{
    $count = getCountArr($arr);
    if ($sort == 'asc') {
        for ($i = 1; $i < $count; $i++) {
            for ($j = 0; $j < $i; $j++) {
                if ($arr[$i] > $arr[$j]) {
                    $x = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $x;
                }
            }
        }
    } elseif ($sort == 'desc') {
        for ($i = 1; $i < $count; $i++) {
            for ($j = 0; $j < $i; $j++) {
                if ($arr[$i] < $arr[$j]) {
                    $x = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $x;
                }
            }
        }
    }
    return $arr;
}

getArrayView(getSortUser([9, 8, 5, 2, 3, 7, 4, 6, 2, 7, 9, 5, 1, 2]));
echo '<br>';
/**
 * @param array $arr1
 * @return string
 */
function getProverkaSortUser($arr1): string
{
    $arr = getSortUser($arr1);
    $count = count($arr);
    $k = $count;
    while ($k != 0) {
        if ($k != $arr[$k - 1]) {
            return 'function is false';
        }
        $k--;
    }
    return 'function is tru';
}

echo getProverkaSortUser([9, 8, 7, 6, 5, 4, 3, 2, 1]);
echo '<br>';

echo 'anonymous<br>';
/**
 * @param array $arr
 * @param string $sort
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$anonymous = function (array $arr, string $sort = 'desc'): array {
    $count = getCountArr($arr);
    if ($sort == 'asc') {
        for ($i = 1; $i < $count; $i++) {
            for ($j = 0; $j < $i; $j++) {
                if ($arr[$i] > $arr[$j]) {
                    $x = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $x;
                }
            }
        }
    } elseif ($sort == 'desc') {
        for ($i = 1; $i < $count; $i++) {
            for ($j = 0; $j < $i; $j++) {
                if ($arr[$i] < $arr[$j]) {
                    $x = $arr[$i];
                    $arr[$i] = $arr[$j];
                    $arr[$j] = $x;
                }
            }
        }
    }
    return $arr;
};

getArrayView($anonymous([9, 8, 5, 2, 3, 7, 4, 6, 2, 7, 9, 5, 1, 2]));
echo 'Рекурсия<br>';
/**
 * @param array  $arr
 * @param string $sort
 * @return void
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getSortRecursion($arr, $sort = 'desc')
{
    if ($arr && $sort == 'desc') {
        getSortRecursionDesc($arr, $sort);
    } elseif ($arr && $sort == 'asc') {
        getSortRecursionAsc($arr, $sort);
    }
}

/**
 * @param array  $arr
 * @param string $sort
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getSortRecursionDesc($arr, $sort = 'desc')
{
    $count = getCountArr($arr);
    if ($arr) {
        if ($sort == 'desc') {
            for ($i = 1; $i < $count; $i++) {
                for ($j = 0; $j < $i; $j++) {
                    if ($arr[$i] < $arr[$j]) {
                        $x = $arr[$i];
                        $arr[$i] = $arr[$j];
                        $arr[$j] = $x;
                    }
                }
            }
        }
        getArrayView($arr);
        return getSortRecursionDesc($arr = []);
    } else {
        return 1;
    }
}

/**
 * @param array  $arr
 * @param string $sort
 * @return array
 */
function getSortRecursionAsc($arr, $sort = 'asc'): array
{
    $count = getCountArr($arr);
    if ($arr) {
        if ($sort == 'asc') {
            for ($i = 1; $i < $count; $i++) {
                for ($j = 0; $j < $i; $j++) {
                    if ($arr[$i] > $arr[$j]) {
                        $x = $arr[$i];
                        $arr[$i] = $arr[$j];
                        $arr[$j] = $x;

                    }
                }
            }
        }
        getArrayView($arr);
        return getSortRecursionAsc($arr = []);
    } else {
        return 1;
    }
}

getSortRecursion([9, 8, 5, 2, 3, 7, 4, 6, 2, 7, 9, 5, 1, 2]);
echo '3. написать функцию поиска в массиве. функция будет принимать два параметра. Первый массив, второй поисковое число. search(arr, find)<br>';
echo 'Пользовательская функция<br>';
/**
 * @param array  $arr
 * @param string $find
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getFindUser($arr, $find): string
{
    $count = getCountArr($arr);
    for ($i = 0; $i < $count; $i++) {

        if ($find == $arr[$i]) {
            return 'Совпадение найдено';
        }
    }
    return 'Совпадение не найдено';
}

echo getFindUser([9, 8, 5, 2, 3, 7, 4, 6, 2, 7, 9, 5, 1, 2], 10);
echo '<br>anonymous<br>';
/**
 * @param array $arr
 * @param string $find
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
$anonymous = function (array $arr, string $find) {
    $count = getCountArr($arr);
    for ($i = 0; $i < $count; $i++) {

        if ($find == $arr[$i]) {
            return 'Совпадение найдено';
        }
    }
    return 'Совпадение не найдено';
};
echo $anonymous([9, 8, 5, 2, 3, 7, 4, 6, 2, 7, 9, 5, 1, 2], 10);

echo '<br>Рекурсия<br>';
/**
 * @param array  $arr
 * @param string $find
 * @return mixed
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getFindRecursion($arr, $find)
{
    $count = getCountArr($arr);
    if ($arr && $find) {
        for ($i = 0; $i < $count; $i++) {
            if ($find == $arr[$i]) {
                return 'Совпадение найдено';
            }
        }
        return getFindRecursion($arr = []);
    } else {
        return 1;
    }
}

echo getFindRecursion([9, 8, 5, 2, 3, 7, 4, 6, 2, 7, 9, 5, 1, 2], 1);
