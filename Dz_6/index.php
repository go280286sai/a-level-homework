<?php
/**
 * @param array $arr
 * @return mixed
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}

echo '1. Вам нужно создать массив и заполнить его случайными числами от 1 до 100 (ф-я rand). Далее, вычислить произведение тех элементов, которые больше нуля и у которых четные индексы. После вывести на экран элементы, которые больше нуля и у которых нечетный индекс.';
$arr = [];
for ($i = 0; $i < 100; $i++) {
    $arr[] = rand(1, 100);
}
/**
 * @param array $arr
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProizEven(array $arr): float
{
    $totalBig = 1;
    for ($i = 0; $i < 10; $i++) {
        if ($arr[$i] > 0 && $i % 2 == 0) {
            $totalBig *= $arr[$i];
        }
    }
    return $totalBig;
}

echo '<br>Произведение с четными индексами: ' . getProizEven($arr);
echo '<br>';
/**
 * @param array $arr
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 * description proverka
 */
function getProverkaProizEven($arr, $n): string
{
    if (getProizEven($arr) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
$test_array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
echo getProverkaProizEven($test_array, 945);
/**
 * @param array $arr
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProizNotEven(array $arr): float
{
    $totalSmall = 1;
    for ($i = 0; $i < 10; $i++) {
        if ($arr[$i] > 0 && $i % 2 != 0) {
            $totalSmall *= $arr[$i];
        }
    }
    return $totalSmall;
}

echo '<br>Произведение с нечетными индексами: ' . getProizNotEven($arr);
echo '<br>';
/**
 * @param array $arr
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaProizNotEven($arr, $n): string
{
    if (getProizNotEven($arr) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
$test_array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
echo getProverkaProizNotEven($test_array, 384);
echo '<br>2. Даны два числа. Найти их сумму и произведение. Даны два числа. Найдите сумму их квадратов.';
/**
 * @param float $a
 * @param float $b
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getMultiNumber($a, $b): float
{
    return $a * $b;
}
echo '<br>Произведение чисел: ' . getMultiNumber(6.34, 5.56);

/**
 * @param float $a
 * @param float $b
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getSumNumber($a, $b): float
{
    return $a + $b;
}
echo '<br>Сумма чисел: ' . getSumNumber(6, 5);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaSumNumber($a, $b, $n): string
{
    if (getSumNumber($a, $b) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaSumNumber(2, 2, 4);
/**
 * @param float $a
 * @param float $b
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getSumKvadrNomber($a, $b): float
{
    return $a ** $b;
}

echo '<br>Сумма квадратов чисел: ' . getSumKvadrNomber(6, 5);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaSumKvadrNomber($a, $b, $n): string
{
    if (getSumKvadrNomber($a, $b) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaSumNumber(2, 2, 4);
echo '<br>3. Даны три числа. Найдите их среднее арифметическое.';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getAvgNomber($a, $b, $c): float
{
    return ($a + $b + $c) / 3;
}

echo '<br> Среднее арифметическое: ' . getAvgNomber(8, 9, 5);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkagetAvgNomber($a, $b, $c, $n): string
{
    if (getAvgNomber($a, $b, $c) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkagetAvgNomber(3, 3, 3, 3);
echo '<br>4. Дано число. Увеличьте его на 30%, на 120%.';
/**
 * @param float $n
 * @param float $proc
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProcentNomber($n, $proc): float
{
    return ($n + ($n * ($proc / 100)));
}

echo '<br>на 30%: ' . getProcentNomber(50, 30);
echo '<br>на 120%: ' . getProcentNomber(50, 120);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaProcentNomber($a, $b, $n): string
{
    if (getProcentNomber($a, $b) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaProcentNomber(50, 30, 65);
echo '<br>5. Пользователь выбирает из выпадающего списка страну (Турция, Египет или Италия), вводит количество дней для 
отдыха и указывает, есть ли у него скидка (чекбокс). Вывести стоимость отдыха, которая вычисляется как произведение 
количества дней на 400. Далее это число увеличивается на 10%, если выбран Египет, и на 12%, если выбрана Италия. И далее 
это число уменьшается на 5%, если указана скидка.';
echo '<form action="index.php" method="post">
        <select name="country">
            <option value="Турция">Турция</option>
            <option value="Египет">Египет</option>
            <option value="Италия">Италия</option>
        </select><br>
        Введите количество дней:<br>
        <input type="number" name="day" id="">
        <br>
        Наличие скидки:
        <input type="checkbox" name="skidka">
        <br>
        <input type="submit" value="submit" name="submit1">
    </form>';
if ($_REQUEST['submit1']) {
    $day = htmlspecialchars($_REQUEST['day']);
    $skidka = $_REQUEST['skidka'];
    $country = $_REQUEST['country'];
    if ($skidka == 'on') {
        $skidka = 0.05;
    } else {
        $skidka = 0;
    }
    if ($country == 'Египет') {
        $country = 0.1;
    } elseif ($country == 'Италия') {
        $country = 0.12;
    } else {
        $country = 0;
    }
    echo 'Стоимость отдыха: ' . getLeaveDays($skidka, $country, $day);
}
/**
 * @param float $skidka
 * @param float $country
 * @param float $day
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getLeaveDays($skidka, $country, $day): float
{
    return $day * 400 + $day * 400 * $country - $day * 400 * $country * $skidka;
}

echo '<br>6. Пользователь вводит свой имя, пароль, email. Если вся информация указана, то показать эти данные после фразы 
"Регистрация прошла успешно", иначе сообщить какое из полей оказалось не заполненным.' . '<br>';
if ($_REQUEST['submit']) {
    $email = htmlspecialchars($_REQUEST['email']);
    $name = htmlspecialchars($_REQUEST['name']);
    $password = htmlspecialchars($_REQUEST['password']);
    getTrueForm($name, $email, $password);
}
/**
 * @param string $a
 * @param string $b
 * @param string $c
 * @return void
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getTrueForm($a, $b, $c)
{
    $mas = ['Не введено имя пользователя' => $a, 'Не введен email' => $b, 'Не введен пароль' => $c];
    $coun = 0;
    foreach ($mas as $index => $item) {
        if (!$item) {
            echo $index . '<br>';
            $coun++;
        }
    }
    if ($coun == 0) {
        $mas = ['Имя пользователя' => $a, 'Введен email' => $b, 'Введен пароль' => $c];
        echo '<br>Регистрация прошла успешно<br>';
        foreach ($mas as $index => $item) {
            echo $index . ': ' . $item . '<br>';
        }
    }
}

echo '<form action="index.php>" method="post">
        Имя:<br>
        <input type="text" name="name"><br>
        Адресс почты:<br>
        <input type="email" name="email"><br>
        Пароль:<br>
        <input type="password" name="password"><br>
        <input type="submit" name="submit" value="Отправить">
    </form>';
echo '<br>7. Выведите на экран n раз фразу "Silence is golden". Число n вводит пользователь на форме. Если n некорректно, вывести фразу "Bad n".' . '<br>';
if ($_REQUEST['submit3']) {
    $n = htmlspecialchars($_REQUEST['chislo']);
    if (is_numeric($n)) {
        getTextN($n);
    } else {
        echo 'Bad n';
    }
}
echo '<form action="index.php">
        Введите число:<br>
        <input type="text" name="chislo"><br>
        <input type="submit" name="submit3" value="submit">
    </form>';
/**
 * @param float $n
 * @return void
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getTextN($n)
{
    for ($i = 0; $i < $n; $i++) {
        echo 'Silence is golden' . '<br>';
    }
}

echo '<br>8. Заполнить массив длины n нулями и единицами, при этом данные значения чередуются, начиная с нуля.' . '<br>';
/**
 * @param float $n
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getArrayLenght($n): array
{
    $k = 0;
    $mas = [0, 1];
    $arr = [];
    while ($k != $n) {
        for ($i = 0; $i < 2; $i++) {
            $arr[] = $mas[$i];
        }
        $k++;
    }

    return $arr;
}

getArrayView(getArrayLenght(8));
echo '<br>9. Определите, есть ли в массиве повторяющиеся элементы.' . '<br>';
/**
 * @param array $arr
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getRepetElement(array $arr): array
{
    $k = 0;
    $mas = [];
    $count = count($arr);
    while ($k != $count) {
        $t = 0;
        for ($i = 0; $i < $count; $i++) {
            if ($arr[$k] == $arr[$i]) {
                $t++;
            }

        }
        if ($t > 1 && !in_array($arr[$k], $mas)) {
            $mas[] = $arr[$k];
        }
        $k++;
    }
    return $mas;
}

getArrayView(getRepetElement([1, 2, 3, 1, 2, 5, 8, 7, 5, 2, 4, 1, 2]));
echo '<br>10. Найти минимальное и максимальное среди 3 чисел' . '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getMinElement($a, $b, $c): float
{
    $min = $a;
    if ($min > $b) {
        $min = $b;
    }
    if ($min > $c) {
        $min = $c;
    }
    return $min;
}

/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getMaxElement($a, $b, $c): float
{
    $min = $a;
    if ($min < $b) {
        $min = $b;
    }
    if ($min < $c) {
        $min = $c;
    }
    return $min;
}

echo 'Минимальное значение: ' . getMinElement(3, 2, 1);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaMinElement($a, $b, $c, $n): string
{
    if (getMinElement($a, $b, $c) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaMinElement(1, 2, 3, 1);
echo '<br>';
echo 'Максимальное значение: ' . getMaxElement(1, 2, 3);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaMaxElement($a, $b, $c, $n): string
{
    if (getMaxElement($a, $b, $c) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaMaxElement(1, 2, 3, 3);
echo '<br>11. Найти площадь(трапеция)' . '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $h
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getSTrapezoid($a, $b, $h): float
{
    return 0.5 * ($a + $b) * $h;
}

echo getSTrapezoid(10, 15, 6);
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaSTrapezoid($a, $b, $c, $n): string
{
    if (getSTrapezoid($a, $b, $c) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaSTrapezoid(10, 15, 6, 75);
echo '<br>12. Теорема Пифагора' . '<br>';
/**
 * @param float $a
 * @param float $b
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getTeorPiphagor($a, $b): float
{
    $c = $a ** 2 + $b ** 2;
    return $c ** 0.5;
}

echo getTeorPiphagor(6, 8);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaTeorPiphagor($a, $b, $n): string
{
    if (getTeorPiphagor($a, $b) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaTeorPiphagor(6, 8, 10);
echo '<br>13. Найти периметр' . '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return float
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getPtriangle($a, $b, $c): float
{
    return $a + $b + $c;
}

echo getPtriangle(5, 5, 5);
echo '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @param float $n
 * @return string
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getProverkaPtriangle($a, $b, $c, $n): string
{
    if (getPtriangle($a, $b, $c) == $n) {
        return 'function is tru';
    } else {
        return 'function is false';
    }
}
echo getProverkaPtriangle(5, 5, 5, 15);
echo '<br>14. Найти дискриминант' . '<br>';
/**
 * @param float $a
 * @param float $b
 * @param float $c
 * @return void
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getDiscriminant($a, $b, $c)
{
    $discr = $b ** 2 - 4 * $a * $c;
    echo 'Дискриминант D = ' . $discr . '<br>';
    if ($discr > 0) {
        $x1 = (-$b + ($discr * 0.5)) / (2 * $a);
        $x2 = (-$b - ($discr * 0.5)) / (2 * $a);
        echo 'x1 = ' . $x1 . ' ' . 'x2 = ' . $x2;
    } elseif ($discr == 0) {
        $x = -$b / (2 * $a);
        echo 'x = ' . $x;
    } else {
        echo 'Корней нет';
    }
}

getDiscriminant(2, 4, 2);
echo '<br>15. Создать только четные числа до 100' . '<br>';
/**
 * @param float $n
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getEvenNumber($n): array
{
    $mas = [];
    for ($i = 0; $i < $n; $i++) {
        if ($i % 2 == 0) {
            $mas[] = $i;
        }
    }
    return $mas;
}

getArrayView(getEvenNumber(100));
echo '<br>15. Создать не четные числа до 100 ' . '<br>';
/**
 * @param mixed $n
 * @return array
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getNotEvenNumber($n): array
{
    $mas = [];
    for ($i = 0; $i < $n; $i++) {
        if ($i % 2 != 0) {
            $mas[] = $i;
        }
    }
    return $mas;
}

getArrayView(getNotEvenNumber(100));
