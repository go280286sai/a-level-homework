/*1. Получить от юзера число.
Получить сумму квадаров всех чисел от 1 до числа, которое ввел юзер. Пример:
Юзер ввел 4
(1 * 1) + (2 * 2) + (3 * 3) + (4 * 4) = 30
Вывести в консоль результат
Привести во вторую степерь можно через оператор **. 3 ** 2 = 9 */
let n = 4;
let total = 0;
for (let i = 0; i <= n; i++) {
    total += (i ** i);
}
console.log(total);

/*2. Есть массив [3, 5, 12, 9, 23, 93, 17]
Отфильтровать его так, чтобы остались только те числа, которые больше 2 и меньше 20. И потом получить их сумму.*/
let arr = [3, 5, 12, 9, 23, 93, 17];
let sum_t = (a, b) => a + b;
let arr2 = arr.filter(el => el > 2 && el < 20).reduce(sum_t);
console.log(arr2);

/*3.Дан массив [[1, 6, 3, '6'], [10, 15, 13, '10']]. Найти сумму элементов, которые являются числами и которые кратны двум */
arr = [[1, 6, 3, '6'], [10, 15, 13, '10']];
let mas = new Array();
sum_t = (a, b) => a + b;
for (let i = 0; i < arr.length; i++) {
    mas.push(arr[i].filter(el => typeof (el) == 'number').reduce(sum_t));
}
console.log(mas.reduce(sum_t));

/*4. Написать функцию, которая устанавливает новые свойства в объект.
Функция принимает в себя 3 аргумента - key, value, obj
key - свойство, которое хотим добавить. Принимаем это от юзера.
value - значение свойства. Принимаем это от юзера.
obj - объект, в который хотим добавить новое свойство.
Если юзер ввел ключ, который уже есть в объекте, то выводим сообщение - "Уже есть", если ключа нет, то устанавливаем его в объект.*/

const objMutation = (key, value, object) => {
    object[key] = value;
};
const person = {
    name: 'Bobo'
};

/**
 *
 * @param key
 * @param value
 * @param obj
 * @returns obj
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getMewObject(key, value, obj) {
    obj[key] = value;
    return obj;
}

let objec = {
    'one': 1,
    'two': 2,
    'three': 3
};
let newObjec = (key, value, obj) => (obj[key] = value);
console.log(getMewObject('four', 4, objec));
newObjec('five', 5, objec);
console.log(objec);
