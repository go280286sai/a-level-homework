<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add comment</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col">
            <img src="img/header.png" alt="" width="100%">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <form action="db.php" method="post">
                <div class="mb-3">
                    <textarea class="form-control" rows="3" required name="text"
                              placeholder="Введите текст сообщения"></textarea>
                </div>
                <div>
                    <label for="exampleInputEmail1" class="form-label">Email address</label>
                    <input type="email" class="form-control" aria-describedby="emailHelp" name="email"
                           placeholder="Введите Email address" required>
                </div>
                <div>
                    <label for="exampleInputEmail1" class="form-label">Ваше имя</label>
                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Введите Ваше имя"
                           required name="name">
                </div>
                <div>
                    <label class="form-check-label" for="exampleCheck1">Ваша оценка в целом</label></div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="exampleCheck1" name="select" value="Плохо">
                    <label class="form-check-label" for="exampleCheck1">Плохо</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="exampleCheck1" name="select" value="Удовлетварительно">
                    <label class="form-check-label" for="exampleCheck1">Удовлетварительно</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" class="form-check-input" id="exampleCheck1" name="select" value="Отлично" checked>
                    <label class="form-check-label" for="exampleCheck1">Отлично</label>
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <img src="img/footer.png" alt="" width="100%">
        </div>
    </div>
</div>
</body>
</html>
