/*1. Дан объект с городами и странами.
Написать функцию getCity. Эта функция (getCity) должна вернуть новый массив, элементы которого будут преобразованы
в данный формат: <Столица> - это <Страна>.
Доступ к объекту может быть любым (через контекст, напрямую и т.д.)
Можно использовать Object.entries метод )*/
const citiesAndCountries = {
    'Киев': 'Украина',
    'Нью-Йорк': 'США',
    'Амстердам': 'Нидерланды',
    'Берлин': 'Германия',
    'Париж': 'Франция',
    'Лиссабон': 'Португалия',
    'Вена': 'Австрия',
};

function getCity() {
    var arr = [];
    for (el in citiesAndCountries) {
        let stroka = `${el} - это ${citiesAndCountries[el]}`;
        arr.push(stroka);
    }
    console.log(arr);
};
getCity.prototype.obj = function (citiesAndCountries) {
    return citiesAndCountries;
};
const result = getCity(); // ['Киев - это Украина', 'Нью-Йорк - это США', ... и т.д.]

/* Cоздать объект с названиями дней недели. Где ключами будут ru и en, a значением свойства ru будет массив
с названиями дней недели на русском, а en - на английском.
После написать функцию которая будет выводить в консоль название дня недели пользуясь выше созданным объектом
(доступ к объекту можно получить напрямую).
Все дни недели начинаются с 1 и заканичаются цифрой 7 (1- понедельник, 7 - воскресенье).
Функция принимает в аргументы 2 параметра:
lang - название языка дня недели
day - число дня недели
Можно вспомнить про метод indexOf(). А может можно и без него :)*/
const namesOfDays = {
    ru: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
    en: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
}

function getNameOfDay(lang, datNumber) {
    if (lang == 'ru') {
        console.log(namesOfDays[lang][datNumber]);
    }
    if (lang == 'en') {
        console.log(namesOfDays[lang][datNumber]);
    }
}
getNameOfDay.__proto__ = namesOfDays;
getNameOfDay('ru', 2);
getNameOfDay('en', 2);

/*Написать универсальную функцию setProto, которая принимает в себя 2 аргумента (currentObj, protoObj).
Функция должна устанавливать прототип (protoObj) для currentObj. То есть после вызова функции мы должны получить результат:
*/
const person = {
    name: 'Vlad'
};
const person1 = {
    age: 1
};
function setProto (currentObj, protoObj) {
    currentObj.__proto__=protoObj;
}
setProto(person1, person);
// Теперь прототипом для объекта person1 выступает объект person

/*4. Создать базовый объек person. Этот объект должен выступать в роли прототипа для объекта person1.
В объекте person должны быть такие методы:
метод для установки имени и возвраста (setName, setAge)
метод для получения имени и возвраста (getName, getAge)
метод для валидации возраста (ageValidation)*/
const persons ={
    setAge : function (age) {
         this.age=this.ageValidation(age);
    },
    setName : function (name) {
        this.name=name;
    },
    getAge : function () {
        console.log(this.age);
    },
    getName : function () {
        console.log(this.name);
    },
    ageValidation:function (age){
        if(age<18){
            return 'Validation Error';
        }
        else {
            return age;
        }
    }
};
const person3={
    name:'dmitriy',
    age:18
};
person3.__proto__=persons;
person3.setName('viktor'); // установили новое имя
person3.getName(); // имя
person3.setAge(20); // установили возраст
person3.getAge(); // получили возраст

person3.setAge(1); // передать возраст можно как угодно
person3.getAge(); // 'Validation Error'

person3.setAge(30); // передать возраст можно как угодно
person3.getAge(); // Новое значение - 20
