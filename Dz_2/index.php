<?php
$name = 'Alexandr';
echo $name.'<br>';
$age = 35;
echo $age.'<br>';
$pi = 3.14;
echo $pi.'<br>';
$arr1 = [['alex', 'vova', 'tolya']];
echo $arr1;
echo '<br>';
$arr2 = ['alex', 'vova', 'tolya', ['kostya', 'olya']];
echo $arr2;
echo '<br>';
$arr3 = ['alex', 'vova', 'tolya', ['kostya', 'olya', ['gosha', mila]]];
echo $arr3;
echo '<br>';
$arr4 = [['alex', 'vova', 'tolya'], ['kostya', 'olya'], ['gosha', mila]];
echo $arr4;
