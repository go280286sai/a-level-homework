<?php
session_start();
if ($_REQUEST['submit']) {
    if (!isset($_SESSION['true']) && !isset($_SESSION['false'])) {
        $_SESSION['true'] = 0;
        $_SESSION['false'] = 0;
    }
    $chislo = rand(5, 8);
    if ($_REQUEST['text'] == $chislo) {
        $_SESSION['true']++;
        echo 'Вы угадали ';
        echo '<a href="form.php">Играть еще</a>';
        echo '<br>';
        echo 'Статистика игры:' . '<br>';
        echo 'Угадано: ' . $_SESSION['true'] . ' Не угадано: ' . $_SESSION['false'];

    } elseif ($_REQUEST['text'] >= 5 && $_REQUEST['text'] <= 8 && $_REQUEST['text'] != $chislo) {
        $_SESSION['false']++;
        echo 'Не угадано ';
        echo '<a href="form.php">Попробуйте еще</a>';
        echo '<br>';
        echo 'Статистика игры:' . '<br>';
        echo 'Угадано: ' . $_SESSION['true'] . ' Не угадано: ' . $_SESSION['false'];
    } elseif ($_REQUEST['text'] < 5) {
        $_SESSION['false']++;
        echo 'Число маленькое ';
        echo '<a href="form.php">Попробуйте еще</a>';
        echo '<br>';
        echo 'Статистика игры:' . '<br>';
        echo 'Угадано: ' . $_SESSION['true'] . ' Не угадано: ' . $_SESSION['false'];
    } elseif ($_REQUEST['text'] > 8) {
        $_SESSION['false']++;
        echo 'Число большое ';
        echo '<a href="form.php">Попробуйте еще</a>';
        echo '<br>';
        echo 'Статистика игры:' . '<br>';
        echo 'Угадано: ' . $_SESSION['true'] . ' Не угадано: ' . $_SESSION['false'];
    }
} else {
    echo '<a href="form.php">Играть</a>';
}
