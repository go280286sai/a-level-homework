<?php
/**
 * @param array $arr
 * @return mixed
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item . '<br>';
    }
}

echo '1. Реализовать свою функцию count()';
$n = 0;
echo '<br>' . 'For:';
for ($i = 0; $i < 100; $i++) {
    $n++;
}
echo '<br>' . $n;
echo '<br>' . 'While:';
$n = 0;
while ($n < 100) {
    $n++;
}
echo '<br>' . $n;
echo '<br>' . 'do:';
$n = 0;
do {
    $n++;
} while ($n < 100);
echo '<br>' . $n;
echo '<br>' . 'Foreach:';
$n = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
$k = 0;
foreach ($n as $item) {
    $k++;
}
echo '<br>' . $k . '<br>';
echo '2. Дан массив ["Alex", "Vanya", "Tanya", "Lena", "Tolya"]. Развернуть этот массив в обратном направлении.';
$n = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
echo '<br>' . 'For:' . '<br>';
$m = 0;
$mas = [];
for ($i = 0; $n[$i] != null; $i++) {
    $m++;
}
for ($i = $m - 1; $i >= 0; $i--) {
    $mas[] = $n[$i];
}
getArrayView($mas);
echo '<br>';
echo '<br>' . 'While:' . '<br>';
$k = 0;
while ($n[$k] != null) {
    $k++;
}
$mas = [];
while ($k > 0) {
    $mas[] = $n[$k - 1];
    $k--;
}
getArrayView($mas);
echo '<br><br>' . 'do:' . '<br>';
$k = 0;
do {
    $k++;
} while ($n[$k] != null);
$mas = [];
do {
    $mas[] = $n[$k - 1];
    $k--;
} while ($k != 0);
getArrayView($mas);
echo '<br>';
echo '<br>' . 'Foreach:' . '<br>';
$k = 0;
$mas = [];
foreach ($n as $item) {
    $k++;
}
foreach ($n as $item) {
    $mas[$k] = $item;
    $k--;
}
getArrayView($mas);
echo '<br>';

echo '3. Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69]. Развернуть этот массив в обратном направлении.';
$n = [44, 12, 11, 7, 1, 99, 43, 5, 69];
echo '<br>' . 'For:' . '<br>';
$m = 0;
$mas = [];
for ($i = 0; $n[$i] != null; $i++) {
    $m++;
}
for ($i = $m; $i > 0; $i--) {
    $mas[] = $n[$i - 1];
}
getArrayView($mas);
echo '<br>';
echo '<br>' . 'While:' . '<br>';
$k = 0;
while ($n[$k] != null) {
    $k++;
}
$mas = [];
while ($k != 0) {
    $mas[] = $n[$k - 1];
    $k--;
}
getArrayView($mas);
echo '<br><br>' . 'do:' . '<br>';
$k = 0;
do {
    $k++;
} while ($n[$k] != null);
$mas = [];
do {
    $mas[] = $n[$k - 1];
    $k--;
} while ($k != 0);
getArrayView($mas);
echo '<br>';
echo '<br>' . 'Foreach:' . '<br>';
$k = 0;
$mas = [];
foreach ($n as $item) {
    $k++;
}
foreach ($n as $item) {
    $mas[$k] = $item;
    $k--;
}
getArrayView($mas);
echo '<br>';
echo '4. Дана строка str = "Hi I am ALex". Развернуть строку в обратном направлении.';
$n = 'Hi I am ALex';
echo '<br>' . 'For:' . '<br>';
$m = 0;
$mas = '';
for ($i = 0; $n[$i] != null; $i++) {
    $m++;
}
for ($i = $m; $i >= 0; $i--) {
    $mas .= $n[$i];
}
getArrayView($mas);
echo '<br>';
echo '<br>' . 'While:' . '<br>';
$k = 0;
while ($n[$k] != null) {
    $k++;
}
$mas = '';
while ($k != -1) {
    $mas .= $n[$k];
    $k--;
}
getArrayView($mas);
echo '<br><br>' . 'do:' . '<br>';
$k = 0;
do {
    $k++;
} while ($n[$k] != null);
$mas = '';
$arr = [];
do {
    $mas .= $n[$k];
    $arr[] = $n[$k];
    $k--;
} while ($k != -1);
getArrayView($mas);
echo '<br>';
echo '<br>' . 'Foreach:' . '<br>';
$k = 0;
$mas = '';
foreach ($arr as $item) {
    $mas .= $item;
}
getArrayView($mas);
echo '<br>';
echo '5. Дана строка. готовую функцию toUpperCase() or tolowercase(). str = "Hi I am ALex", сделать ее с с маленьких букв.';
$alpha_l = 'abcdefghijklmnopqrstuvwxyz ';
$alpha_b = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$alphabet = [
    'A' => 'a',
    'B' => 'b',
    'C' => 'c',
    'D' => 'd',
    'E' => 'e',
    'F' => 'f',
    'G' => 'g',
    'H' => 'h',
    'I' => 'i',
    'J' => 'j',
    'K' => 'k',
    'L' => 'l',
    'M' => 'm',
    'N' => 'n',
    'O' => 'o',
    'P' => 'p',
    'Q' => 'q',
    'R' => 'r',
    'S' => 's',
    'T' => 't',
    'U' => 'u',
    'V' => 'v',
    'W' => 'w',
    'X' => 'x',
    'Y' => 'y',
    'Z' => 'z',];
$n = 'Hi I am ALex';
echo '<br>' . 'For:' . '<br>';
$m = 0;
$a = 0;
$mas = '';
for ($i = 0; $n[$i] != null; $i++) {
    $m++;
}
for ($i = 0; $alpha_l[$i] != null; $i++) {
    $a++;
}
for ($i = 0; $i <= $m; $i++) {
    $t = 0;
    for ($j = 0; $j <= $a; $j++) {
        if ($n[$i] == $alpha_l[$j]) {
            $mas .= $n[$i];
        } else {
            $t++;
            if ($t == 1) {
                $mas .= $alphabet[$n[$i]];
            } else {
                continue;
            }
        }

    }
}
getArrayView($mas);
echo '<br>' . 'While:' . '<br>';
$k = 0;
$arr = [];
$arr2 = [];
while ($n[$k] != null) {
    $arr[] = $n[$k];
    $k++;
}
$a = 0;
while ($alpha_l[$a] != null) {
    $arr2[] = $alpha_l[$a];
    $a++;
}
$mas = '';
$b = 0;
while ($b != $k) {
    $t = 0;
    $s = 0;
    while ($t != $a) {
        if ($n[$b] == $alpha_l[$t]) {
            $mas .= $n[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$n[$b]];
            }
        }
        $t++;
    }
    $b++;
}
getArrayView($mas);
echo '<br>' . 'do While:' . '<br>';
$k = 0;
do {
    $k++;
} while ($n[$k] != null);
$a = 0;
do {
    $a++;
} while ($alpha_l[$a] != null);
$mas = '';
$b = 0;
do {
    $t = 0;
    $s = 0;
    do {
        if ($n[$b] == $alpha_l[$t]) {
            $mas .= $n[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$n[$b]];
            }
        }
        $t++;
    } while ($t != $a);
    $b++;
} while ($b != $k);
getArrayView($mas);
echo '<br>' . 'Foreach:' . '<br>';
$mas = '';
foreach ($arr as $item) {
    $s = 0;
    foreach ($arr2 as $element) {
        if ($item == $element) {
            $mas .= $item;
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$item];
            }
        }
    }
}
getArrayView($mas);
echo '6. Дана строка str = "Hi I am ALex", сделать все буквы большие.';
$alpha_l = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
$alpha_b = 'abcdefghijklmnopqrstuvwxyz';
$alphabet = [
    'a' => 'A',
    'b' => 'B',
    'c' => 'C',
    'd' => 'D',
    'e' => 'E',
    'f' => 'F',
    'g' => 'G',
    'h' => 'H',
    'i' => 'I',
    'j' => 'J',
    'k' => 'K',
    'l' => 'L',
    'm' => 'M',
    'n' => 'N',
    'o' => 'O',
    'p' => 'P',
    'q' => 'Q',
    'r' => 'R',
    's' => 'S',
    't' => 'T',
    'u' => 'U',
    'v' => 'V',
    'w' => 'W',
    'x' => 'X',
    'y' => 'Y',
    'z' => 'Z',];
$n = 'Hi I am ALex';
echo '<br>' . 'For:' . '<br>';
$m = 0;
$a = 0;
$mas = '';
for ($i = 0; $n[$i] != null; $i++) {
    $m++;
}
for ($i = 0; $alpha_l[$i] != null; $i++) {
    $a++;
}
for ($i = 0; $i <= $m; $i++) {
    $t = 0;
    for ($j = 0; $j <= $a; $j++) {
        if ($n[$i] == $alpha_l[$j]) {
            $mas .= $n[$i];
        } else {
            $t++;
            if ($t == 1) {
                $mas .= $alphabet[$n[$i]];
            } else {
                continue;
            }
        }
    }
}
getArrayView($mas);
echo '<br>' . 'While:' . '<br>';
$k = 0;
$arr = [];
$arr2 = [];
while ($n[$k] != null) {
    $arr[] = $n[$k];
    $k++;
}
$a = 0;
while ($alpha_l[$a] != null) {
    $arr2[] = $alpha_l[$a];
    $a++;
}
$mas = '';
$b = 0;
while ($b != $k) {
    $t = 0;
    $s = 0;
    while ($t != $a) {
        if ($n[$b] == $alpha_l[$t]) {
            $mas .= $n[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$n[$b]];
            }
        }
        $t++;
    }
    $b++;
}
getArrayView($mas);
echo '<br>' . 'do While:' . '<br>';
$k = 0;
do {
    $k++;
} while ($n[$k] != null);
$a = 0;
do {
    $a++;
} while ($alpha_l[$a] != null);
$mas = '';
$b = 0;
do {
    $t = 0;
    $s = 0;
    do {
        if ($n[$b] == $alpha_l[$t]) {
            $mas .= $n[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$n[$b]];
            }
        }
        $t++;
    } while ($t != $a);
    $b++;
} while ($b != $k);
getArrayView($mas);
echo '<br>' . 'Foreach:' . '<br>';
$mas = '';
foreach ($arr as $item) {
    $s = 0;
    foreach ($arr2 as $element) {
        if ($item == $element) {
            $mas .= $item;
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$item];
            }
        }
    }
}
getArrayView($mas);
echo '8. Дан массив ["Alex", "Vanya", "Tanya", "Lena", "Tolya"], сделать все буквы с маленькой.';
$alpha_l = 'abcdefghijklmnopqrstuvwxyz ';
$alpha_b = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
$alphabet = [
    'A' => 'a',
    'B' => 'b',
    'C' => 'c',
    'D' => 'd',
    'E' => 'e',
    'F' => 'f',
    'G' => 'g',
    'H' => 'h',
    'I' => 'i',
    'J' => 'j',
    'K' => 'k',
    'L' => 'l',
    'M' => 'm',
    'N' => 'n',
    'O' => 'o',
    'P' => 'p',
    'Q' => 'q',
    'R' => 'r',
    'S' => 's',
    'T' => 't',
    'U' => 'u',
    'V' => 'v',
    'W' => 'w',
    'X' => 'x',
    'Y' => 'y',
    'Z' => 'z',];
$n = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
echo '<br>' . 'For:' . '<br>';
$m = 0;
$a = 0;
$arr = [];
$arr1 = [];
$arr2 = [];
for ($i = 0; $n[$i] != null; $i++) {
    $kol++;
}
$stroka = '';
for ($i = 0; $i <= $kol; $i++) {
    $stroka .= $n[$i] . ' ';
}
$mas = '';

for ($i = 0; $stroka[$i] != null; $i++) {
    $m++;
    $arr1[] = $stroka[$i];
}

for ($i = 0; $alpha_l[$i] != null; $i++) {
    $a++;
    $arr2[] = $alpha_l[$i];
}
for ($i = 0; $i <= $m; $i++) {
    $t = 0;
    for ($j = 0; $j <= $a; $j++) {
        if ($stroka[$i] == $alpha_l[$j]) {
            $mas .= $stroka[$i];
        } else {
            $t++;
            if ($t == 1) {
                $mas .= $alphabet[$stroka[$i]];
            } else {
                continue;
            }
        }
    }
}
$t = '';
for ($i = 0; $i <= $m; $i++) {
    if ($mas[$i] == ' ') {
        $arr[] = $t;
        $t = '';
    } else {
        $t .= $mas[$i];
    }
}
$arr3 = [];
for ($i = 0; $i <= $m; $i++) {
    if ($arr[$i] != null) {
        $arr3[] = $arr[$i];
    }
}
getArrayView($arr3);
echo '<br>' . 'While:' . '<br>';
$k = 0;
$arr = [];
$stroka = '';
while ($n[$k] != null) {
    $k++;
}
$t = 0;
while ($t != $k) {
    $stroka .= $n[$t] . ' ';
    $t++;
}
$k = 0;
while ($stroka[$k] != null) {
    $k++;
}
echo $stroka;
$a = 0;
while ($alpha_l[$a] != null) {
    $a++;
}
$mas = '';
$b = 0;
while ($b != $k) {
    $t = 0;
    $s = 0;
    while ($t != $a) {
        if ($stroka[$b] == $alpha_l[$t]) {
            $mas .= $stroka[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$stroka[$b]];
            }
        }
        $t++;
    }
    $b++;
}
$arr = [];
$b = 0;
$t = '';
while ($b != $k) {
    $s = 0;
    if ($mas[$b] == ' ') {
        $arr[] = $t;
        $t = '';
    } else {
        $t .= $mas[$b];
    }
    $b++;
}
getArrayView($arr);

echo '<br>' . 'do While:' . '<br>';
$k = 0;
$arr = [];
$stroka = '';

do {
    $k++;
} while ($n[$k] != null);
$t = 0;
do {
    $stroka .= $n[$t] . ' ';
    $t++;
} while ($t != $k);
$k = 0;
do {
    $k++;
} while ($stroka[$k] != null);
echo $stroka;
$a = 0;
do {
    $a++;
} while ($alpha_l[$a] != null);
$mas = '';
$b = 0;
do {
    $t = 0;
    $s = 0;
    while ($t != $a) {
        if ($stroka[$b] == $alpha_l[$t]) {
            $mas .= $stroka[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$stroka[$b]];
            }
        }
        $t++;
    }
    $b++;
} while ($b != $k);
$arr = [];
$b = 0;
$t = '';
do {
    $s = 0;
    if ($mas[$b] == ' ') {
        $arr[] = $t;
        $t = '';
    } else {
        $t .= $mas[$b];
    }

    $b++;
} while ($b != $k);

getArrayView($arr);

echo '<br>' . 'Foreach:' . '<br>';
$mas = [];
$newArr = [];
foreach ($arr1 as $item) {
    $s = 0;
    foreach ($arr2 as $element) {
        if ($item == $element) {

            $newArr[] = $item;
        } else {
            $s++;
            if ($s == 1) {
                $newArr[] = $alphabet[$item];
            }
        }
    }
}
$arr = [];
foreach ($newArr as $item) {
    if ($item != null) {
        $arr[] = $item;
    }
}
$text = '';
foreach ($arr as $item) {
    if ($item == ' ') {
        $mas[] = $text;
        $text = '';
    } else {
        $text .= $item;
    }
}
$mas2 = [];
foreach ($mas as $item) {
    if ($item != null) {
        $mas2[] = $item;
    }
}
getArrayView($mas2);
echo '9. Дан массив ["Alex", "Vanya", "Tanya", "Lena", "Tolya"], сделать все буквы с большой.';
$alpha_l = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ ';
$alpha_b = 'abcdefghijklmnopqrstuvwxyz';
$alphabet = [
    'a' => 'A',
    'b' => 'B',
    'c' => 'C',
    'd' => 'D',
    'e' => 'E',
    'f' => 'F',
    'g' => 'G',
    'h' => 'H',
    'i' => 'I',
    'j' => 'J',
    'k' => 'K',
    'l' => 'L',
    'm' => 'M',
    'n' => 'N',
    'o' => 'O',
    'p' => 'P',
    'q' => 'Q',
    'r' => 'R',
    's' => 'S',
    't' => 'T',
    'u' => 'U',
    'v' => 'V',
    'w' => 'W',
    'x' => 'X',
    'y' => 'Y',
    'z' => 'Z',];
$n = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
echo '<br>' . 'For:' . '<br>';
$m = 0;
$a = 0;
$arr = [];
$arr1 = [];
$arr2 = [];
for ($i = 0; $n[$i] != null; $i++) {
    $kol++;
}
$stroka = '';
for ($i = 0; $i <= $kol; $i++) {
    $stroka .= $n[$i] . ' ';
}
$mas = '';

for ($i = 0; $stroka[$i] != null; $i++) {
    $m++;
    $arr1[] = $stroka[$i];
}
for ($i = 0; $alpha_l[$i] != null; $i++) {
    $a++;
    $arr2[] = $alpha_l[$i];
}
for ($i = 0; $i <= $m; $i++) {
    $t = 0;
    for ($j = 0; $j <= $a; $j++) {
        if ($stroka[$i] == $alpha_l[$j]) {
            $mas .= $stroka[$i];
        } else {
            $t++;
            if ($t == 1) {
                $mas .= $alphabet[$stroka[$i]];
            } else {
                continue;
            }
        }

    }
}
$t = '';
for ($i = 0; $i <= $m; $i++) {
    if ($mas[$i] == ' ') {
        $arr[] = $t;
        $t = '';
    } else {
        $t .= $mas[$i];
    }
}
$mas1 = [];
for ($i = 0; $i <= $m; $i++) {
    if ($arr[$i] != null) {
        $mas1[] = $arr[$i];

    }
}

getArrayView($mas2);
echo '<br>' . 'While:' . '<br>';
$k = 0;
$arr = [];
$stroka = '';
while ($n[$k] != null) {
    $k++;
}
$t = 0;
while ($t != $k) {
    $stroka .= $n[$t] . ' ';
    $t++;
}
$k = 0;
while ($stroka[$k] != null) {
    $k++;
}
$a = 0;
while ($alpha_l[$a] != null) {
    $a++;
}
$mas = '';
$b = 0;
while ($b != $k) {
    $t = 0;
    $s = 0;
    while ($t != $a) {
        if ($stroka[$b] == $alpha_l[$t]) {
            $mas .= $stroka[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$stroka[$b]];
            }
        }
        $t++;
    }
    $b++;
}
$arr = [];
$b = 0;
$t = '';
while ($b != $k) {
    $s = 0;
    if ($mas[$b] == ' ') {
        $arr[] = $t;
        $t = '';
    } else {
        $t .= $mas[$b];
    }

    $b++;
}
getArrayView($arr);

echo '<br>' . 'do While:' . '<br>';
$k = 0;
$arr = [];
$stroka = '';

do {
    $k++;
} while ($n[$k] != null);
$t = 0;
do {
    $stroka .= $n[$t] . ' ';
    $t++;
} while ($t != $k);
$k = 0;
do {
    $k++;
} while ($stroka[$k] != null);
$a = 0;
do {
    $a++;
} while ($alpha_l[$a] != null);
$mas = '';
$b = 0;
do {
    $t = 0;
    $s = 0;
    while ($t != $a) {
        if ($stroka[$b] == $alpha_l[$t]) {
            $mas .= $stroka[$b];
        } else {
            $s++;
            if ($s == 1) {
                $mas .= $alphabet[$stroka[$b]];
            }
        }
        $t++;
    }
    $b++;
} while ($b != $k);
$arr = [];
$b = 0;
$t = '';
do {
    $s = 0;
    if ($mas[$b] == ' ') {
        $arr[] = $t;
        $t = '';
    } else {
        $t .= $mas[$b];
    }

    $b++;
} while ($b != $k);

getArrayView($arr);

echo '<br>' . 'Foreach:' . '<br>';
$mas = [];
$newArr = [];
foreach ($arr1 as $item) {
    $s = 0;
    foreach ($arr2 as $element) {
        if ($item == $element) {
            $newArr[] = $item;
        } else {
            $s++;
            if ($s == 1) {
                $newArr[] = $alphabet[$item];
            }
        }
    }
}
$arr = [];
foreach ($newArr as $item) {
    if ($item != null) {
        $arr[] = $item;
    }
}
$text = '';
foreach ($arr as $item) {
    if ($item == ' ') {
        $mas[] = $text;
        $text = '';
    } else {
        $text .= $item;
    }
}
$mas2 = [];
foreach ($mas as $item) {
    if ($item != null) {
        $mas2[] = $item;
    }
}
getArrayView($mas2);

echo '10. Дано число num = 1234678, развернуть ее в обратном направлении.';
$n = 1234678;
$n = (string)$n;
echo '<br>' . 'For:' . '<br>';
$m = 0;
$mas = '';
for ($i = 0; $n[$i] != null; $i++) {
    $m++;
}
for ($i = $m; $i >= 0; $i--) {
    $mas .= $n[$i];
}
getArrayView($mas);
echo '<br>';
echo '<br>' . 'While:' . '<br>';
$k = 0;
while ($n[$k] != null) {
    $k++;
}
$mas = '';
while ($k != -1) {
    $mas .= $n[$k];
    $k--;
}
getArrayView($mas);
echo '<br><br>' . 'do:' . '<br>';
$k = 0;
do {
    $k++;
} while ($n[$k] != null);
$mas = '';
$arr = [];
do {
    $mas .= $n[$k];
    $arr[] = $n[$k];
    $k--;
} while ($k != -1);
getArrayView($mas);
echo '<br>';
echo '<br>' . 'Foreach:' . '<br>';
$k = 0;
$mas = '';
foreach ($arr as $item) {
    $mas .= $item;
}
getArrayView($mas);
echo '<br>';
echo '11. Дан массив [44, 12, 11, 7, 1, 99, 43, 5, 69], отсортируй его в порядке убывания.';
$arr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
/**
 * @param array $arr
 * @return mixed
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getCountArr(array $arr): mixed
{
    $getCount = 0;
    for ($i = 0; $arr[$i] != null; $i++) {
        $getCount++;
    }
    return $getCount;
}

$countArray = getCountArr($arr);
echo '<br>';
echo '<br>' . 'While:' . '<br>';
$i = 0;
while ($i < $countArray) {
    $j = $i - 1;
    $key = $arr[$i];
    $i++;
    while ($arr[$j] < $key && $j >= 0) {
        $arr[$j + 1] = $arr[$j];
        $j--;
        $arr[$j + 1] = $key;

    }

}
getArrayView($arr);
echo '<br><br>' . 'do:' . '<br>';
$i = $countArray - 1;
do {
    $j = $i - 1;
    $key = $arr[$i];
    $i++;
    do {
        $arr[$j + 1] = $arr[$j];
        $j--;
        $arr[$j + 1] = $key;
    } while ($arr[$j] < $key && $j >= 0);
} while ($i < $countArray);
getArrayView($arr);
echo '<br>' . 'Foreach:' . '<br>';
$i = 0;
foreach ($arr as $item) {
    if ($i < $countArray) {
        $j = $i - 1;
        $key = $item;
        $i++;
    }
    foreach ($arr as $item) {
        if ($arr[$j] < $key && $j >= 0) {
            $arr[$j + 1] = $arr[$j];
            $j--;
            $arr[$j + 1] = $key;
        }
    }
}
getArrayView($arr);
echo '<br>' . 'For:' . '<br>';
for ($i = 1; $i < $countArray; $i++) {
    for ($j = 0; $j < $i; $j++) {
        if ($arr[$i] > $arr[$j]) {
            $x = $arr[$i];
            $arr[$i] = $arr[$j];
            $arr[$j] = $x;
        }
    }
}
getArrayView($arr);
