<?php
/**
 * @param array $arr
 * @return mixed
 * @author Alexander Storchak <go280286sai@gmail.com>
 */
function getArrayView(array $arr)
{
    foreach ($arr as $item) {
        echo $item.'<br>';
    }
}
echo '1. Написать программу, которая выводит простые числа, т.е. делящиеся без остатка только на себя и на 1.';
$n = 1;
$arr = [];
while ($n <= 100) {
    $k = 0;
    for ($i = 1; $i <= $n; $i++) {
        if ($n % $i == 0) {
            $k ++;
        }
    }
    if ($k == 2) {
        $arr[] = $n;
    }
    $n++;
}
getArrayView($arr);
echo '2. Сгенерируйте 100 раз новое число и выведите на экран количество четных чисел из этих 100.';
$n = 0;
$k = 0;
while ($n <= 100) {
    if ($n % 2 == 0) {
        $k ++;
    }
    $n++;
}
echo '<br>' . $k . '<br>';
echo '3. Сгенерируйте 100 раз число от 1 до 5 и выведите на экран сколько раз сгенерировались эти числа (1, 2, 3, 4 и 5).';
$n = 0;
$k = 0;
while ($n <= 100) {
    for ($i = 1; $i <= 5; $i++) {
        $k ++;
    }
    $n++;
}
echo '<br>' . $k . '<br>';
echo '4. Используя условия и циклы сделать таблицу в 5 колонок и 3 строки (5x3), отметить разными цветами часть ячеек.' . '<br>';
echo '<table border="2">';
for ($n = 0; $n < 3; $n++) {
    echo '<tr>';
    for ($m = 0; $m < 5; $m++) {
        $arr = ['1', '2', '3', '4', '5', '0'];
        shuffle($arr);
        $color = implode('', $arr);
        echo '<td bgcolor="#' . $color . '">TEXT</td>';
    }
    echo '</tr>';
}
echo '</table>';
echo '<br>' . '5. В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот месяц (зима, лето, весна, осень).' . '<br>';
$month = rand(1, 12);
switch (true) {
    case ($month == 1 || $month == 2 || $month == 12):
        echo 'Зима';
        break;
    case ($month == 3 || $month == 4 || $month == 5):
        echo 'Весна';
        break;
    case ($month == 6 || $month == 7 || $month == 8):
        echo 'Лето';
        break;
    case ($month == 9 || $month == 10 || $month == 11):
        echo 'Осень';
        break;
}
echo '<br>' . '6. Дана строка, состоящая из символов, например, abcde. Проверьте, что первым символом этой строки является буква a. Если это так - выведите да, в противном случае выведите нет. <br>';
$str = 'abcde';
$str = str_split($str);
shuffle($str);
$text = implode('', $str);
if ($text[0] == 'a') {
    echo 'да';
} else {
    echo 'нет';
}
echo '<br>' . "7. Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3. Если это так - выведите 'да', в противном случае выведите 'нет'." . '<br>';
$str = '12345';
$str = str_split($str);
shuffle($str);
$text = implode('', $str);
if ($text[0] == '1' || $text[0] == '2' || $text[0] == '3') {
    echo 'да';
} else {
    echo 'нет';
}
echo '<br>' . "8. Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при test, равном true, false. Напишите два варианта скрипта - тернарка и if else." . '<br>';
$arr = [true, false];
$test = array_rand($arr, 1);
if ($test) {
    echo 'Верно';
} else {
    echo 'Неверно';
}
echo '<br>';
echo $test ? 'Верно' : 'Неверно';
echo '<br>' . "9. Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']. Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделать через if else и через тернарку." . '<br>';
$mas = ['ru', 'en'];
$lang = array_rand($mas, 1);
$arrRu = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'];
$arrEn = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su'];
if ($lang == 'ru') {
    getArrayView($arrRu);
} else {
    getArrayView($arrEn);
}
echo '<br>';
($lang == 'ru') ? getArrayView($arrRu) : getArrayView($arrEn);
echo '<br>' . '10. В переменной cloсk лежит число от 0 до 59 – это минуты. Определите в какую четверть часа попадает это число (в первую, вторую, третью или четвертую). тернарка и if else.' . '<br>';
$clock = rand(0, 59);
if ($clock >= 0 && $clock <= 15) {
    echo 'Первая четверть';
} elseif ($clock > 15 && $clock <= 30) {
    echo 'Вторая четверть';
} elseif ($clock > 30 && $clock <= 45) {
    echo 'Третья четверть';
} else {
    echo 'Четвертая четверть';
}
echo '<br>';
echo ($clock >= 0 && $clock <= 15) ? 'Первая четверть' : (($clock > 15 && $clock <= 30) ? 'Вторая четверть' : (($clock > 30 && $clock <= 45) ? 'Третья четверть' : 'Четвертая четверть'));
