const area = document.getElementById('area');
let move = 0;
let result='';
let allTotal;
let total=localStorage.getItem('allTotal');
let krestTotal;
let krest_total=localStorage.getItem('krestTotal');
let nullTotal;
let null_total=localStorage.getItem('nullTotal');
let around;
const contentWrapper = document.getElementById('content');
const btnClose = document.getElementById('btn-close');
const round = document.getElementById('round');
const users = document.getElementById('users');
const resetGame=document.getElementById('resetGame');
console.log(total);
console.log(null_total);
console.log(krest_total);

if(total==null){
    total=0;
}
if(krest_total==null){
    krest_total=0;
}
if(null_total==null){
    null_total=0;
}
area.addEventListener('click', e => {
    if (e.target.className = 'box') {
        move % 2 === 0 ? e.target.innerHTML = 'X' : e.target.innerHTML = '0';
        move++;
        check();
    }
})
const check = () => {
    const boxes = document.getElementsByClassName('box');
    const arr=[
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [0,4,8],
        [2,4,6]
    ];
    for(i=0;i<arr.length;i++){
        if(
        boxes[arr[i][0]].innerHTML=='X'&& boxes[arr[i][1]].innerHTML=='X'&&boxes[arr[i][2]].innerHTML=='X'
        ){
            result='крестики';
            krest_total++;
            localStorage.setItem('krestTotal', krest_total);
            prepareResult(result);
        }else if(boxes[arr[i][0]].innerHTML=='0'&& boxes[arr[i][1]].innerHTML=='0'&&boxes[arr[i][2]].innerHTML=='0'
        ){
            result='нолики';
            null_total++;
            localStorage.setItem('nullTotal', null_total);
            prepareResult(result);
        }
    }
}
const prepareResult=winner=>{
contentWrapper.innerHTML=`Победили: ${winner}!`;
}
const closeModal=()=>{
    total++;
    console.log(total);
    localStorage.setItem('allTotal', total);
    location.reload();
}
const resetGameAll=()=>{
    localStorage.setItem('krestTotal', 0);
    localStorage.setItem('nullTotal', 0);
    localStorage.setItem('allTotal', 0);
    location.reload();
};
resetGame.addEventListener('click', resetGameAll);
btnClose.addEventListener('click', closeModal);
round.innerHTML=`Раунд: ${total}`;
users.innerHTML=`Игрок1: ${krest_total} - ${null_total} Игрок2`